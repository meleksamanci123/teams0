module WorksHelper
	# Вспомогателиный метод для отображения кол-ва бригад, соответствующих определенному тэгу.
	def tagged_teams_count(tag)
		Team.tagged_with(tag).count
	end
end
