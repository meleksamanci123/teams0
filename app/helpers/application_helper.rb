module ApplicationHelper
	# Вспомогательный метод для создания ссылок, которые подразумевают фильтрацию по параметру.
	# Как устроена фильтрация можно посмотреть в TeamController, метод index.
	def filter(column, title, value=nil)
		value ||= title
		link_to title, { controller: :teams, action: :index, filter: 'filter', column: column.downcase, value: value }
	end
end
