Teams::Application.routes.draw do

  root to: 'welcome#index'

  resources :countries
  resources :teams do
    # Дополнительный маршрут для получения списка тэгов для автодополнения.
    get :autocomplete_works_name, :on => :collection    
  end
  resources :works, model: "Tag", only: [:index]

end
