Given /^I have countries named (.+)$/ do |names|
  names.split(', ').each do |name|
    Country.create!(name: name)
  end
end

Given /^I have no countries$/ do
  Country.delete_all
end

Then /^i should have (\d+) country$/ do |count|
  Country.count.should == count.to_i
end