Feature: Manage Countries
	In order to make a country
	As an user
	I want to create and manage countries

	@base @create
	Scenario: Countries List
		Given I have countries named Russia, Moldova
		When I visit the countries page
		Then I should see Russia
		And I should see Moldova

	@valid @create
	Scenario: Create Valid Country
		Given I have no countries
		And I am on the countries page
		When I follow "Add new country"
		And I fill in "country" "name" with Moldova
	 	And I press "OK"
	 	Then I should see New country added successfully!
	 	And I should see Moldova
	 	And i should have 1 country