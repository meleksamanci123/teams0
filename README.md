THIS IS TASK ABOUT A CONSTRUCTION TEAMS AND TAGS
================================================

## Possible solutions for tables
 1. You can use gem [DataGrid](https://github.com/bogdan/datagrid), [demo](http://datagrid.heroku.com/).
 2. You can use JQuery plugin [DataTables](https://github.com/DataTables/DataTables) or gem [jquery-datatables-rails](https://github.com/rweng/jquery-datatables-rails).
 3. Custom sortable columns and filters in tables.

### I chose the last.

## Solution for tags
 1. I used gem [acts-as-taggable-on](https://github.com/mbleigh/acts-as-taggable-on),
 2. gem [rails3-jquery-autocomplete](https://github.com/crowdint/rails3-jquery-autocomplete)
 3. and gem [jquery-ui-rails](https://github.com/joliss/jquery-ui-rails) for the previous.

### I used [Twitter Bootstrap](http://twitter.github.com/bootstrap/) for pretty views.
### Small truble: tags creating dont't work in non latin characters. Can be solved.

## How to use.
 1. Cucumber test contein tests fot javascript autocomplete and filtering, they are commented in manage_teams.feature.
 2. Home page is '/'. Start rails server and point your browser to http://localhost:3000/.
 3. For see indexes of models click on model name from home page.
 4. For use filters or sorting click on the name of column.

